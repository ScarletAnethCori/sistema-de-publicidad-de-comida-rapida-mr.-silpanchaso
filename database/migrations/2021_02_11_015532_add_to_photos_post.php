<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddToPhotosPost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->string('photo')->default('https://scontent.flpb1-1.fna.fbcdn.net/v/t1.0-9/118771818_116080333550901_1212753296756052001_o.jpg?_nc_cat=100&ccb=3&_nc_sid=730e14&_nc_ohc=2wG3Yc8OCIoAX_HuSDF&_nc_ht=scontent.flpb1-1.fna&oh=192e67018c517ee22d8a7a696a4764cb&oe=604EADAE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn('photo');
            //
        });
    }
}
